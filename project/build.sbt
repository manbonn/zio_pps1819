ThisBuild / scalaVersion     := "2.12.10"
ThisBuild / version          := "0.1"
ThisBuild / organization     := "it.unibo.pss1819.bonarrigo"
ThisBuild / organizationName := "pps1819"

lazy val zioVersion = "1.0.0-RC17"
lazy val scalaFxVersion = "8.0.144-R12"
lazy val root = (project in file("."))
  .settings(
    name := "ZIO-Project",
    libraryDependencies += "org.scalafx" %% "scalafx" % scalaFxVersion,
    libraryDependencies += "dev.zio" %% "zio" % zioVersion,
    libraryDependencies += "org.apache.commons" % "commons-math3" % "3.2",
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio-test"     % zioVersion % "test",
      "dev.zio" %% "zio-test-sbt" % zioVersion % "test"
    ),
    testFrameworks ++= Seq(new TestFramework("zio.test.sbt.ZTestFramework"))
  )