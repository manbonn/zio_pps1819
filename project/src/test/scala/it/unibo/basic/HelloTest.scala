package it.unibo.basic

import it.unibo.pps1819.bonarrigo.zio.basic.StandardHello
import zio.test.Assertion.equalTo
import zio.test.environment.TestConsole
import zio.test.{DefaultRunnableSpec, assert, suite, testM}

object HelloTest extends DefaultRunnableSpec(
  suite("HelloTestSuite")(
    testM("Hello correctly displays output") {
      for {
        _ <- StandardHello.run(List())
        output <- TestConsole.output
      } yield assert(output, equalTo(Vector("Hello, World!\n")))
    },
    testM("Hello returns 0") {
      for {
        output <- StandardHello.run(List())
      } yield assert(output, equalTo(0))
    }
  )
)
