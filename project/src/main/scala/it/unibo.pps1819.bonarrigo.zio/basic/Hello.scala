package it.unibo.pps1819.bonarrigo.zio.basic

import it.unibo.pps1819.bonarrigo.zio.basic.Hello.subject
import zio._
import zio.console._

object Hello extends App{

  val subject = "PPS1819"

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] = {
    val a = unsafeRun(StandardHello.run(List()))
    val b = unsafeRun(ParametricHello.run(List()))
    val c = unsafeRun(PureHello.run(List()))

    UIO(a+b+c)
  }
}

object StandardHello extends App {
  val hello: ZIO[Console, Nothing, Unit] =
    putStrLn("Hello, World!")

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    hello as 1
}

object ParametricHello extends App {
  def hello(greetings: String): ZIO[Console, Nothing, Unit] =
    putStrLn(s"Hello, $greetings!")

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    hello(subject) as 1
}

object PureHello extends App {
  def hello(greetings: UIO[String]): ZIO[Console, Nothing, Unit] =
    greetings.flatMap(s => putStrLn(s"Hello, $s!"))

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] = {
    hello(UIO(subject)) as 1
  }
}
