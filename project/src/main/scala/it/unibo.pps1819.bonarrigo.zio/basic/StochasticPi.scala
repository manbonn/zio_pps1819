package it.unibo.pps1819.bonarrigo.zio.basic

import it.unibo.pps1819.bonarrigo.zio.ThreadUtils
import zio._
import zio.random._
import zio.console._

object StochasticPi extends App{

  private val approximatePi = 3.1415
  private val defaultApproximation = 1.0E-5

  final case class PiState(inside: Ref[Long], total: Ref[Long], value: Ref[Double])
  final case class Point(x: Double, y: Double)

  def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    workplace as 0

  private val randomPoint: ZIO[Random, Nothing, Point] =
    for{
      x <- nextDouble
      y <- nextDouble
    } yield Point(x, y)

  private val initState: ZIO[ZEnv, Nothing, PiState] =
    for {
      inside <- Ref.make(0L)
      total <- Ref.make(0L)
      value <- Ref.make(0.0000)
    } yield PiState(inside, total, value)

  private def toPiConvergence(state: PiState): Schedule[Any, Unit, Unit] =
    Schedule.doUntilM[Unit](_ => state.value.get.map(v => compareDouble(v, approximatePi, defaultApproximation)))

  private val workplace =
    for {
      state <- initState
      workers = assignWork(state)
      _ <- ZIO.collectAllPar(workers)
      _ <- publisher(state.value)
    } yield()

  private def assignWork(state: PiState)=
    ZIO.replicate(ThreadUtils.cores)(worker(state).flatMap( _ =>
      publisher(state.value)).repeat(
        toPiConvergence(state)))

  private def worker(state: PiState): ZIO[ZEnv, Nothing, Unit] =
    for {
      point <- randomPoint
      _ <- pointAnalyzer(point, state)
//      _ <- incrementInside(state).when(insideCircle(point))
      _ <- incrementTotal(state)
      _ <- updatePiValue(state)
    } yield ()

  private def publisher(value: Ref[Double]): ZIO[ZEnv, Nothing, Unit] =
    value.get.map(_.toString) >>= putStrLn

  private def pointAnalyzer(point: Point, state:PiState): UIO[Long] = {
    if (insideCircle(point.x, point.y))
      state.inside.update(pts => pts+1)
    else
      state.inside.get
  }

  private def incrementInside(state: PiState): UIO[Long] = {
    state.total.update(inside => inside + 1)
  }

  private def incrementTotal(state: PiState): UIO[Long] = {
    state.total.update(tot => tot + 1)
  }

  private def updatePiValue(state: PiState): ZIO[Any, Nothing, Unit] =
    for{
      inside <- state.inside.get
      total <- state.total.get
      _ <- state.value.update(_ => estimatePi(inside, total))
    } yield()

  private def compareDouble(d1: Double, d2: Double, precision: Double): Boolean =
    if ((d1 - d2).abs < precision) true else false

  private def estimatePi(inside: Long, total: Long): Double =
    (inside.toDouble / total.toDouble) * 4.0

  private def insideCircle(point: Point): Boolean =
    insideCircle(point.x, point.y)

  private def insideCircle(x: Double, y: Double): Boolean =
    Math.sqrt(x * x + y * y) <= 1.0

}
