package it.unibo.pps1819.bonarrigo.zio.basic

import zio._
import zio.console._
import java.io.IOException
import it.unibo.pps1819.bonarrigo.zio.Utils._

//object ConsoleInteraction extends App{

//  private val triggerableInteraction =
//    for {
//
//    }

//}

object VerboseConsoleInteraction extends App {
  import SilentConsoleInteraction._

  private val verboseInteraction: ZIO[Console, Nothing, Unit] =
    errorProneInteraction.flatMap(text => print(text)).foldM(
      exception => putStrLn(s"IO failed due to [${exception.getMessage}]"),
      _ => UIO()
    )

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    verboseInteraction as 0
}

object SilentConsoleInteraction extends App {

  val errorProneInteraction: ZIO[Console, IOException, String] =
    for {
      _ <- putStrLn("Input a text to be printed:")
      text <- getStrLn
    } yield text

  def print(text: String): ZIO[Console, Throwable, Unit] =
    commandString(text).flatMap(t => putStrLn(s"Your text is $t"))

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    errorProneInteraction.flatMap(text => print(text)).fold(
      _ => 1,
      _ => 0
    )
}

