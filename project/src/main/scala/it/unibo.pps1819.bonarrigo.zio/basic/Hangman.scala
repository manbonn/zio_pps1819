package it.unibo.pps1819.bonarrigo.zio.basic

import zio._
import zio.console._
import zio.random._
import it.unibo.pps1819.bonarrigo.zio.Utils._
import net.degoes.zio.Dictionary

object Hangman extends App {

  def run(args: List[String]): ZIO[ZEnv, Nothing, Int] = {
    program as 0 orDie
  }

  private lazy val untilSuccess: Schedule[Any, Any, Int] =
    Schedule.forever

  private def untilEndOfTheGame(state: Ref[State]): Schedule[Any, Unit, Unit] =
    for {
      _ <- Schedule.doUntilM[Unit](_ => state.get.map(s => s.playerLost || s.playerWon))
    } yield ()

  private val program: ZIO[Console with Random, Exception, Unit] =
    for {
      name <- getName.retry(untilSuccess)
      secret <- chooseWordFromDictionary
      state <- Ref.make(State(name, Set(), secret))
      _ <- putStrLn(s"The secret is $secret")
      _ <- putStrLn("Game start")
      _ <- gameLoop(state).foldM(e => ZIO.fail(e), s => analyzeState(s, state)).repeat(untilEndOfTheGame(state))
      _ <- state.get.flatMap(s => renderState(s))
    } yield()

  private def analyzeState(step: GuessResult, state: Ref[State]): URIO[Console, Unit] =
    step match {
      case GuessResult.Won =>
        for {
          s <- state.get
          _ <- prettySpacedWon(s.name)
        } yield()
      case GuessResult.Lost =>
        for {
          s <- state.get
          _ <- prettySpacedLost(s.word)
        } yield()
      case _ =>
        for {
          s <- state.get
          _ <- renderState(s)
        } yield()
    }

  private def gameLoop(stateRef: Ref[State]): ZIO[Console, Exception, GuessResult] =
    for {
      oldState <- stateRef.get
      userGuess <- getChoice.retry(Schedule.forever)
      newState <- stateRef.update(_ => oldState.addChar(userGuess.toLower))
    } yield guessResult(oldState, newState, userGuess)

  private lazy val getChoice: ZIO[Console, Exception, Char] =
    for {
      _ <- putStrLn("Please enter a character")
      guess <- getStrLn >>= sanitizeInputChar
      _ <- putStrLn(guess.toString)
    } yield guess

  private lazy val getName: ZIO[Console, Exception, String] =
    for {
      _ <- putStrLn("Please enter your name")
      name <- getStrLn >>= sanitizeInputString
    } yield name

  private lazy val chooseWordFromDictionary: URIO[Random, String] =
    nextInt(Dictionary.Dictionary.length).map(index => Dictionary.Dictionary(index))

  private def renderState(state: State): ZIO[Console, Nothing, Unit] = {
    val word =
      state.word.toList
        .map(c => if (state.guesses.contains(c)) s" $c " else "   ")
        .mkString("")
    val line = List.fill(state.word.length)(" - ").mkString("")
    val guesses = " Guesses: " + state.guesses.mkString(", ")
    val text = word + "\n" + line + "\n\n" + guesses + "\n"
    putStrLn(text)
  }

  private def guessResult(oldState: State, newState: State, char: Char): GuessResult =
    if (oldState.guesses.contains(char)) GuessResult.Unchanged
    else if (newState.playerWon) GuessResult.Won
    else if (newState.playerLost) GuessResult.Lost
    else if (oldState.word.contains(char)) GuessResult.Correct
    else GuessResult.Incorrect

  sealed trait GuessResult

  object GuessResult {
    case object Won extends GuessResult
    case object Lost extends GuessResult
    case object Correct extends GuessResult
    case object Incorrect extends GuessResult
    case object Unchanged extends GuessResult
  }

  final case class State(name: String, guesses: Set[Char], word: String) {
    def failures: Int = (guesses -- word.toSet).size
    def playerLost: Boolean = failures > 10
    def playerWon: Boolean = (word.toSet -- guesses).isEmpty
    def addChar(char: Char): State = copy(guesses = guesses + char)
  }

  private def prettySpacedWon(name: String): ZIO[Console, Nothing, Unit] =
    for {
      _ <- putStrLn("").repeat(Schedule.recurs(6))
      _ <- putStrLn(s"Congratulation $name, you won!")
    } yield()

  private def prettySpacedLost(secret: String): ZIO[Console, Nothing, Unit] =
    for {
      _ <- putStrLn("").repeat(Schedule.recurs(6))
      _ <- putStrLn(s"You lost. The word was $secret")
    } yield()
}