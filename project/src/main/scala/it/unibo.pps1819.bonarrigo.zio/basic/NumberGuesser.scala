package it.unibo.pps1819.bonarrigo.zio.basic

import zio._
import zio.random._
import zio.console._
import java.io.IOException
import it.unibo.pps1819.bonarrigo.zio.Utils._


object NumberGuesser extends App {

  case class State(secret: Int, won:Ref[Boolean])

  def run(args: List[String]): ZIO[Console with Random, Nothing, Int] =
    program as 0 orDie

  private val program: ZIO[Console with Random, Throwable, Unit] =
    for {
      state <- initState
      _ <- putStrLn(s"(The secret is ${state.secret.toString})")
      _ <- game(state).repeat(Schedule.doUntilM(_ => state.won.get))
    } yield()

  private def initState =
    for {
      secret <- nextInt(100)
      gameState <- Ref.make(false)
    } yield State(secret, gameState)

  private def game(state: State): ZIO[Console with Random, Throwable, Unit] =
    for {
      input <- acquireUserInput.flatMap(sanitizeInputNumber).retry(Schedule.forever)
      verdict <- analyzeGuess(state.secret, input)
      _ <- state.won.update(_ => verdict)
      _ <- printGameState(verdict)
    } yield()

  private val acquireUserInput: ZIO[Console, IOException, String] =
    for {
      _ <- putStrLn("Make your guess!")
      input <- getStrLn
    } yield input

  private def printGameState(won: Boolean): ZIO[Console, Nothing, Unit] =
    if (won)
      putStrLn("Correct answer!")
    else
      putStr(s"Wrong answer! ")

  private def analyzeGuess(random: Int, guess: Int): UIO[Boolean] =
    UIO(random == guess)
}