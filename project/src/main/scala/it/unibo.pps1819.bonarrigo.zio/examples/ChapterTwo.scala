package it.unibo.pps1819.bonarrigo.zio.examples

import zio._
import zio.random._

class ChapterTwo {}

object ZIODelegation extends App {
  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    ZIODelegate.run(List())
}

object ZIODelegate extends App {
  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    UIO(OutputCodes.answerToEverything)
}

object ExternalCaller extends scala.App {
  val zioRuntime = new DefaultRuntime{}
  val a = zioRuntime.unsafeRun(FishyZIOCode.safe)
//  val b = zioRuntime.unsafeRun(FishyZIOCode.fail) //throws exception
//  val c = zioRuntime.unsafeRun(FishyZIOCode.die) //throws exception
//  println(a)
//  println(b)
//  println(c)

  try{
      val kindaSafe = zioRuntime.unsafeRun(FishyZIOCode.fail)
  } catch {
    case e: Throwable => println("Still, the intended failure type was String, and that information is lost")
//    case e: Throwable => println(e.toString)
  }

  try{
      val kindaSafeButPointless = zioRuntime.unsafeRun(FishyZIOCode.die)
  } catch {
    case e: Throwable => println("There is no compile time information about the type of Throwable. " +
                                  "A case should be specified for every one")
//    case e: Throwable => println(e.toString)
  }
}

object FishyZIOCode {
  val safe: UIO[Int] = UIO(OutputCodes.answerToEverything)
  val fail: IO[String, Nothing] = ZIO.fail("A zio.FiberFailure will be thrown, plus the error in the ZIO will be " +
    "notified explaining the failure. If uncaught, JVM shutdown")
  val die: UIO[Nothing] = ZIO.dieMessage("A java.lang.RuntimeException will be thrown. This is the message payload. If " +
    "uncaught, JVM shutdown")
}

object UioUsage extends App {
  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    args match {
      case empty if empty.isEmpty => UIO(OutputCodes.emptyInputArguments)
      case _ => UIO(OutputCodes.success)
    }
}

object UrioUsage extends App {
  val randomInteger: URIO[Random, Int] =
    nextInt

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    randomInteger
}

object TaskUsage extends App {
  def thrower(condition: Boolean): Task[Int] =
    if (condition)
      Task.succeed(OutputCodes.success)
    else
      Task.fail(new IllegalArgumentException)

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    thrower(false).foldM(
      error => UIO(OutputCodes.silencedException),
      success => UIO(success)
    )
}

object RioUsage extends App {

  val failOnOdd: RIO[Random, Int] =
    nextInt.flatMap(isOdd).foldM(
      _ => RIO.fail(new IllegalStateException),
      success => RIO.succeed(success)
    )

  def isOdd(i: Int): ZIO[Any, Unit, Int] =
    if (i%2 == 0)
      UIO(i)
    else
      ZIO.fail()

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    failOnOdd.foldM(
      _ => UIO(OutputCodes.silencedException),
      _ => UIO(OutputCodes.success)
    )
}

object IoUsage extends App{

  def failOnOddLength(s: String): IO[String, Boolean] =
    isOdd(s.length).foldM(
      _ => IO.fail("fail"),
      success => IO.succeed(true)
    )

  def isOdd(i: Int): ZIO[Any, Unit, Int] =
    if (i%2 == 0)
      UIO(i)
    else
      ZIO.fail()

  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    failOnOddLength("zio").foldM(
      error => UIO(error.length),
      _ => UIO(OutputCodes.success)
    )
}

object EitherLiftingUsage extends App {
  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    program as 0 orDie

  val program: ZIO[ZEnv, Throwable, Int] = Task.succeed(OutputCodes.answerToEverything)
  val eitherProgram: ZIO[ZEnv, Nothing, Either[Throwable, Int]] = program.either
  val reconstituedProgram: ZIO[ZEnv, Throwable, Int] = eitherProgram.absolve
}

object DefectAbsorbingUsage extends App {

  val validNumber: UIO[Int] = UIO(OutputCodes.answerToEverything)
  val invalidExpression: UIO[Int] = validNumber.map(_ / 0)
  val absorbedException: ZIO[Any, Nothing, Int] =
    invalidExpression.absorb.catchAll(
      e => UIO(OutputCodes.silencedException)
    )

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    absorbedException
}

object OutputCodes {
  val success = 0
  val emptyInputArguments = 1
  val silencedException = 2
  val answerToEverything = 42
}