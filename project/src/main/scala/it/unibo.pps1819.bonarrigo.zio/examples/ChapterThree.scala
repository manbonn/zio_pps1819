package it.unibo.pps1819.bonarrigo.zio.examples

import zio._
import zio.console._

class ChapterThree {}

object FiberInterleaving extends App{
  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    program as 0

  val letters: ZIO[Console, Nothing, Unit] =
    for {
      _ <- putStrLn("a")
      _ <- putStrLn("b")
      _ <- putStrLn("c")
    } yield()

  val numbers: ZIO[Console, Nothing, Unit] =
    for {
      _ <- putStrLn("1")
      _ <- putStrLn("2")
      _ <- putStrLn("3")
    } yield()

  val whileLetters: ZIO[Console, Nothing, Nothing] =
    putStrLn("abc").forever

  val whileNumbers: ZIO[Console, Nothing, Nothing] =
    putStrLn("123").forever

  val program: ZIO[Console, Nothing, Unit] =
    for {
      lFiber <- letters.fork
      nFiber <- numbers.fork
      _ <- lFiber.join
      _ <- nFiber.join
    } yield()
}

object FiberFairness extends App {
  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    program as 0

  val whileLetters: ZIO[Console, Nothing, Nothing] =
    putStrLn("abc").forever

  val whileNumbers: ZIO[Console, Nothing, Nothing] =
    putStrLn("123").forever

  val program: ZIO[Console, Nothing, Unit] =
    for {
      lFiber <- whileLetters.fork
      nFiber <- whileNumbers.fork
      _ <- lFiber.join
      _ <- nFiber.join
    } yield()
}

object ScheduleUsage extends App{
  import zio.duration._
  import zio.Schedule._

  private val effect = putStrLn("Print")

  private val forever_ =
    effect.repeat(forever)

  private val never_ =
    effect.repeat(never)

  private val onceMore =
    effect.repeat(once)

  private val fiveTimes =
    effect.repeat(recurs(5))

  private val oneSecondSpacing =
    effect.repeat(spaced(1.second))

  private val stopAfter =
    effect.repeat(duration(1.second))

  private val longestDelayBetween =
    effect.repeat(spaced(200.millis) && recurs(5))

  private val shortestDelayBetween =
    effect.repeat(spaced(200.millis) || recurs(5))

  private val whileCondition =
    effect.repeat(doWhile(_ => true))

  private val whileMonadicCondition =
    effect.repeat(doUntilM(_ => UIO(true)))

  private val untilCOndition =
    effect.repeat(doUntil(_ => false))

  private val untilMonadicCOndition =
    effect.repeat(doUntilM(_ => UIO(false)))

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    forever_ as 0
}