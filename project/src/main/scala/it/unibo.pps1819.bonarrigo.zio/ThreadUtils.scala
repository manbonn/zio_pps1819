package it.unibo.pps1819.bonarrigo.zio

object ThreadUtils {
  val cores: Int = Runtime.getRuntime.availableProcessors()

  def detachExecution(f: Unit => Unit): Unit =
    new Thread{
      override def run(): Unit = {
        f.apply()
      }
    }.start()
}