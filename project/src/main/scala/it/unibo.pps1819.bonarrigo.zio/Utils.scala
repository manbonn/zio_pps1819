package it.unibo.pps1819.bonarrigo.zio

import java.io.IOException

import zio.{UIO, ZIO}

object Utils {

  def sanitizeInputNumber(input: String): ZIO[Any, IllegalArgumentException, Int] =
    input.trim match {
      case number if number.matches("^\\d+$") => UIO(number.toInt)
      case nan if nan.matches("^\\D+$") => ZIO.fail(new IllegalArgumentException("Input is not a number"))
      case empty if empty.matches("^\\s+$") => ZIO.fail(new IllegalArgumentException("No input detected"))
      case _ => ZIO.fail(new IllegalArgumentException("Input is just invalid"))
    }

  def sanitizeInputString(input: String): ZIO[Any, IllegalArgumentException, String] =
    input.trim match {
      case word if word.matches("^\\D+$") => UIO(word)
      case empty if empty.matches("^\\s+$") => ZIO.fail(new IllegalArgumentException("No input detected"))
      case _ => ZIO.fail(new IllegalArgumentException("Input is just invalid"))
    }

  def sanitizeInputChar(input: String): ZIO[Any, IllegalArgumentException, Char] =
    sanitizeInputString(input).foldM(
      error => ZIO.fail(error),
      success => UIO(success.charAt(0))
    )

  def commandString(input: String): ZIO[Any, Throwable, String] =
    sanitizeInputString(input).foldM(
      e => ZIO.fail(e),
      {
        case trigger if trigger == "throw" => ZIO.fail(new IOException("Triggered IOException"))
        case e => UIO(e)
      }
    )


}
