package it.unibo.pps1819.bonarrigo.zio.concurrency.production

import zio._
import zio.clock.Clock
import zio.console._
import zio.duration._

object Production extends App {

  private val expectedConsumers = 10
  private val consumerDecrease = 3
  private val initialGoods = expectedConsumers * consumerDecrease * 2
  private val consumingThreshold = consumerDecrease
  private val producingThreshold = consumerDecrease
  private val producerIncrease = consumerDecrease * expectedConsumers
  private val inspectionTime = 2000.millis
  private val permits: Long = expectedConsumers + 1

  type Goods = Int
  case class State(goods: Ref[Goods])

  private def initState(initial: Goods): UIO[State] =
    for {
      ref <- Ref.make(initial)
    } yield State(ref)

  private def initPolicy(initial: Long) =
    for{
      sem <- Semaphore.make(permits)
    } yield sem

  private def publisher(shared: State): URIO[Console, Unit] =
    shared.goods.get.map(n => n.toString) >>= putStrLn

  private def producer(shared: State) =
    shared.goods.update(n => n + producerIncrease) *>
      publisher(shared) whenM
        shared.goods.get.map(g => g <= producingThreshold)

  private def consumer(shared: State) =
    shared.goods.update(n => n - consumerDecrease) *>
      publisher(shared) whenM
        shared.goods.get.map(g => g >= consumingThreshold)

  private val inspector =
    for{
      _ <- putStrLn("Inspection started")
      _ <- ZIO.sleep(inspectionTime)
      _ <- putStrLn("Inspection finished")
    } yield()

  private def waitProducer(producer: Fiber[Nothing, Unit]) =
    producer.join

  private def waitConsumers(consumers: Iterable[ZIO[ZEnv, Nothing, Fiber[Nothing, Unit]]]) =
    ZIO.foreach(consumers) { fiber =>
      fiber >>= (res => res.join)
    }

  private def supervisedProduction(policy: Semaphore, state: State) =
    for{
      _ <- policy.withPermit(producer(state)) repeat Schedule.spaced(500.millis)
    }yield()

  private def supervisedConsumption(policy: Semaphore, state: State) =
    for {
      _ <- policy.withPermit(consumer(state)) repeat Schedule.spaced(200.millis)
    }yield()

  private def supervision(policy: Semaphore) =
    for {
      _ <- policy.withPermits(permits)(inspector) repeat Schedule.spaced(5000.millis)
    }yield()

  private val program =
    for {
      state <- initState(initialGoods)
      policy <- initPolicy(permits)
      scFiber = ZIO.replicate(expectedConsumers)(supervisedConsumption(policy, state).fork)
      spFiber <- supervisedProduction(policy, state).fork
      sFiber <- supervision(policy).fork
      _ <- waitConsumers(scFiber)
      _ <- waitProducer(spFiber)
      _ <- sFiber.join
    }yield()

  override def run(args: List[String]): ZIO[ZEnv, Nothing, Int] =
    program as 0
}