package it.unibo.pps1819.bonarrigo.zio.concurrency.smartpositioning

import javafx.event.{ActionEvent, EventHandler}
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.{JFXApp, Platform}
import scalafx.scene.control.Button
import scalafx.scene.layout._
import scalafx.scene.paint.Color
import scalafx.scene.shape.Circle
import scalafx.scene.{Scene, SubScene}

object Visualization extends JFXApp {
  stage = ViewComponents.mainStage
}

private object ViewComponents {
  private val vBoxMaxHeight = 150
  private val vBoxMaxWidth = 150

  private def publish(particles: Seq[Particle]): Unit =
    Platform.runLater{
      ballScene.getChildren.clear()
      for (logicParticle <- particles) {
        val visibleParticle = particleRender(logicParticle)
        ballScene.getChildren.add(visibleParticle)
      }
    }

  private def play: EventHandler[ActionEvent] =
    _ => {
      Platform.runLater{
        playButton.disable = true
        stopButton.disable = false
        Control.play(publish)
      }
    }

  private def stop: EventHandler[ActionEvent] =
    _ => {
      Platform.runLater{
        stopButton.disable = true
        playButton.disable = false
        Control.stop()
      }
    }

  private def particleRender(particle: Particle): Circle  =
    new Circle {
      layoutX = particle.position.getX + ParticleEnvironment.logicWidth/2
      layoutY = particle.position.getY + ParticleEnvironment.logicHeight/2
      fill = Color.White
      radius = 3
      strokeWidth = 1
      stroke = Color.Black
    }

  private val playButton: Button =
    new Button("Play") {
      minHeight = vBoxMaxHeight
      minWidth = vBoxMaxWidth
      onAction = play
    }

  private val stopButton: Button =
    new Button("Stop") {
      disable = true
      minHeight = vBoxMaxHeight
      minWidth = vBoxMaxWidth
      onAction = stop
    }

  private val ballScene: SubScene =
    new SubScene(800, 600)

  private val sideVBox: VBox =
    new VBox(15, playButton, stopButton) {
      maxWidth = vBoxMaxWidth
    }

  private val mainHBox: HBox =
    new HBox(7, ballScene, sideVBox)

  private val mainScene: Scene =
    new Scene {
      content = mainHBox
    }

  val mainStage: PrimaryStage = new PrimaryStage {
    title = "ScalaSmartPositioning"
    scene = mainScene
  }
}