package it.unibo.pps1819.bonarrigo.zio.concurrency.smartpositioning

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import scala.util.Random.nextDouble

case class Particle(position: Vector2D, velocity: Vector2D, mass: Double, charge: Double)

object ParticleEnvironment {
  val logicWidth: Double = 800
  val logicHeight: Double = 600

  val maximumMass: Double = 50
  val maximumCharge: Double = 20
  val maximumParticles: Int = 100

  val defaultParticles: Int = 100
  val defaultTimeStep: Int = 17
  val defaultIterations: Int = 1000

  val frictionConstant: Double = 0.08
  val universalConstant: Double = 1.0
}

object ParticleFactory{
  import ParticleEnvironment._

  type World = Seq[Particle]
  type Force = Vector2D

  def freshWorld(size: Int): World = (0 until size).map(_ => random)
  def make(position: Vector2D, velocity: Vector2D, mass: Double, charge: Double): Particle = Particle(position, velocity, mass, charge)

  private def randomX: Double = nextDouble*logicWidth - logicWidth/2d
  private def randomY: Double = nextDouble*logicHeight - logicHeight/2d
  private def randomMass: Double = nextDouble*maximumMass + 1
  private def randomCharge: Double = nextDouble*maximumCharge + 1
  private def randomCoordinate = new Vector2D(randomX, randomY)
  private def zeroVelocity = new Vector2D(0, 0)
  private def random: Particle = Particle(randomCoordinate, zeroVelocity, randomMass, randomCharge)
}