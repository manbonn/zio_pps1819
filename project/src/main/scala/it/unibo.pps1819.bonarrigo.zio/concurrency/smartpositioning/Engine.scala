package it.unibo.pps1819.bonarrigo.zio.concurrency.smartpositioning

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

object EngineAdapter {
  import zio.DefaultRuntime
  import scala.concurrent.Promise
  import ZIOEngine._
  import it.unibo.pps1819.bonarrigo.zio.concurrency.smartpositioning.ParticleFactory.World

  private val zioRuntime = new DefaultRuntime{}
  private var interruption = Promise[Boolean]

  def compute(workers: Int, displayFunction: World => Unit): Unit =
    zioRuntime.unsafeRun(program(workers, displayFunction, interruption))

  def terminate(): Unit = {
    interruption.success(true)
    interruption = Promise[Boolean]
  }
}

object ZIOEngine{
  import zio._
  import zio.duration._
  import ParticleFactory._
  import ParticleEnvironment._
  import Schedule.{spaced, doUntil}
  import scala.annotation.tailrec
  import scala.concurrent.Promise

  def program(parallelism: Int, displayFunction: World => Unit, interruption: Promise[Boolean]): ZIO[ZEnv, Nothing, Unit] =
    for {
      world <- Ref.make(freshWorld(defaultParticles))
      _ <- workplace(parallelism, world).andThen(
             publisher(displayFunction, world)).repeat(
               spaced(defaultTimeStep.millis) both doUntil(_ => interruption.isCompleted))
    } yield ()


  private def workplace(parallelism: Int, world: Ref[World]): ZIO[ZEnv, Nothing, Unit] =
    for{
      actualWorld <- world.get
      balancedWork <- balanceWork(parallelism, actualWorld)
      assignedWork = assignWork(balancedWork, actualWorld)
      completedIteration <- ZIO.collectAllPar(assignedWork)
      _ <- world.update(_ => completedIteration.flatten)
    } yield()

  private def balanceWork(workers: Int, world: World): ZIO[Any, Nothing, List[Seq[Particle]]] =
    UIO(world.grouped(workers)).absorb.foldM(
      _ => UIO(world.grouped(1).toList),
      success => UIO(success.toList)
    )

  private def assignWork(work: List[Seq[Particle]], world: World): Seq[ZIO[Any, Nothing, List[Particle]]] =
    for (jobs <- work) yield parallelWorker(jobs, world)

  private def parallelWorker(partialWorld: Seq[Particle], world: World): ZIO[Any, Nothing, List[Particle]] =
    for {
      updatedPartial <- ZIO.foreach(partialWorld) {particle =>
        for {
          force <- forceCalculator(world, particle, Vector2D.ZERO)
          applied <- forceApplier(particle, force)
        } yield applied
      }
    }yield updatedPartial

  private def publisher(displayFunction: World => Unit, world: Ref[World]): ZIO[Any, Nothing, Unit] =
    for {
      actualEnv <- world.get
    } yield displayFunction(actualEnv)

  private def forceApplier(particle: Particle, force: Force): UIO[Particle] =
    UIO(
      particle.copy(
        position = particle.position.add(particle.velocity.scalarMultiply(defaultTimeStep)),
        velocity = particle.velocity.add(force.scalarMultiply(defaultTimeStep).scalarMultiply(1/particle.mass))
      )
    )

  @tailrec
  private def forceCalculator(world: World, particle: Particle, forceAccumulator: Force): UIO[Force] =
    world match {
      case Nil => UIO(forceAccumulator)
      case Seq(head, tail @ _*) =>
        if(particle != head)
          forceCalculator(tail, particle, forceAccumulator.add(forceBetween(particle, head)))
        else
          forceCalculator(tail, particle, forceAccumulator)
    }

  private def distance(particle: Particle, other: Particle): Vector2D =
    particle.position.subtract(other.position)

  private def forceBetween(particle: Particle, other: Particle): Force =
    distance(particle, other).scalarMultiply(universalConstant *((particle.charge * other.charge)/normCubed(particle, other)))

  private def normCubed(particle: Particle, other:Particle): Double =
    Math.pow(distance(particle, other).getNorm, 3)
}