package it.unibo.pps1819.bonarrigo.zio.concurrency.smartpositioning

object Control{
  import it.unibo.pps1819.bonarrigo.zio.ThreadUtils._

  def play(displayFunction: Seq[Particle] => Unit): Unit =
    detachExecution(_ => compute(cores, displayFunction))

  def stop(): Unit =
    detachExecution(_ => terminate())

  private def compute(workers: Int, displayFunction: Seq[Particle] => Unit): Unit =
    EngineAdapter.compute(workers, displayFunction)

  private def terminate(): Unit =
    EngineAdapter.terminate()
}