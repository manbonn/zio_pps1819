package it.unibo.pps1819.bonarrigo.zio.concurrency.dining

import java.util.concurrent.TimeUnit

import zio._
import zio.console._
import zio.duration._

object Dinner extends App{
  private val guests = 10
  private val diningGuests = 0 until guests
  private val eatingTime = Duration(1, TimeUnit.SECONDS)
  private val binaryPermit = 1

  case class Chopstick(name: String, sem: Semaphore)
  case class Both(first: Chopstick, second: Chopstick)

  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] = {
    program as 0
  }

  private val initChopsticks: ZIO[Any, Nothing, Seq[Chopstick]] =
    ZIO.foreach(diningGuests) { index =>
      for {
        sem <- Semaphore.make(binaryPermit)
      } yield Chopstick(index.toString, sem)
    }

  private def initPhilosophers(chopsticks: Seq[Chopstick]): Seq[URIO[ZEnv, Fiber[Nothing, Unit]]] =
    diningGuests.map(philosopher =>
      acquire(philosopher.toString, arrangeChairs(philosopher, chopsticks)).fork)

  private def arrangeChairs(chairIndex: Int, chopsticks:Seq[Chopstick]): Both =
    if (chairIndex == guests - 1)
      Both(chopsticks.head, chopsticks(chairIndex))
    else
      Both(chopsticks(chairIndex), chopsticks(chairIndex + 1))

  private def acquire(philosopher: String, chopsticks: Both): ZIO[ZEnv, Nothing, Unit] = {
    for {
      _ <- chopsticks.first.sem.withPermit(
            chopsticks.second.sem.withPermit(
              eating(philosopher, chopsticks.first.name, chopsticks.second.name)))
    } yield ()
  }

  private def eating(philosopher: String, firstChopstick: String, secondChopstick: String): ZIO[ZEnv, Nothing, Unit] =
    for {
      _ <- putStrLn(s"$philosopher Eating with $firstChopstick and $secondChopstick")
      _ <- ZIO.sleep(eatingTime)
      _ <- putStrLn(s"$philosopher Releasing $firstChopstick and $secondChopstick")
    } yield()

  private val program: ZIO[ZEnv, Nothing, Unit] =
    for {
      chops <- initChopsticks
      phil = initPhilosophers(chops)
      _ <- ZIO.foreachPar(phil) {j =>
        j.flatMap(f => f.join).forever
      }
    } yield()
}